package com.exceptionhandling.exceptionhandling.repository;

import com.exceptionhandling.exceptionhandling.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUserId(int id);
}
