package com.exceptionhandling.exceptionhandling.service;

import com.exceptionhandling.exceptionhandling.dto.UserRequest;
import com.exceptionhandling.exceptionhandling.entity.User;
import com.exceptionhandling.exceptionhandling.exception.UserNotFoundException;
import com.exceptionhandling.exceptionhandling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public User saveUser(UserRequest userRequest) {
        User user = User.build(0, userRequest.getName(), userRequest.getEmail(), userRequest.getMobile(), userRequest.getGender(), userRequest.getAge(), userRequest.getNationality());
        return repository.save(user);
    }

    public List<User> getAllUsers() {
        return repository.findAll();
    }

    public User getUser(int userId) throws UserNotFoundException {
        User user= repository.findByUserId(userId);

        if(user!= null){
            return user;
        }else{
            throw new UserNotFoundException("user not found with id : "+userId);
        }
    }
}
