package com.exceptionhandling.exceptionhandling.controller;

import com.exceptionhandling.exceptionhandling.dto.UserRequest;
import com.exceptionhandling.exceptionhandling.entity.User;
import com.exceptionhandling.exceptionhandling.exception.UserNotFoundException;
import com.exceptionhandling.exceptionhandling.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/save")
    public ResponseEntity<User> saveUser(@RequestBody @Valid UserRequest userRequest) {
        return new ResponseEntity<>(userService.saveUser(userRequest), HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.ok(userService.getAllUsers());
    }


    @GetMapping("/{userId}")
    public ResponseEntity<User> getUser(@PathVariable int userId) throws UserNotFoundException {
        return ResponseEntity.ok(userService.getUser(userId));
    }
}
