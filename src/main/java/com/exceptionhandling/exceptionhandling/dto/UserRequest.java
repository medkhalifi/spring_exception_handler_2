package com.exceptionhandling.exceptionhandling.dto;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
public class UserRequest {
    @NotNull(message = "user name shouldn't be null")
    private String name;
    @Email
    private String email;
    @Pattern(regexp = "^\\d{10}$",message = "Invalide mobile number")
    private String mobile;
    private String gender;
    @Min(18)
    @Max(50)
    private int age;
    @NotBlank
    private String nationality;
}
